function make(elem, args, apendTo, text='', htext='') {
    elem = document.createElement(elem)
    for (let e in args){
        elem.setAttribute(e, args[e])
    }
    elem.innerText = text
    elem.innerHTML = htext
    apendTo.appendChild(elem)
}



function toSingUp() {
    let mainDiv = document.getElementById('input-conteiner')
    let actionDiv = document.getElementById('actions')
    actionDiv.innerHTML = ''
    
    let title = document.getElementById('title')
    title.innerText = 'Sing Up'

    let conteiner = document.createElement('div')
    conteiner.setAttribute('class', 'input-container')

    make('p',
        null, 
        conteiner, 
        '', 
        'Confirm Password')


    make('input', {
        'type': 'password',
        'class': 'inData',
        'id': 'confirmPass'
    }, conteiner)

    make('input',{
        'type': 'button',
        'onClick': 'createUser()',
        'value': 'Sing Up',
        'id': 'btn'
    }, actionDiv)


    make('p', null, actionDiv, '', 'Old user? <a href="login.html">Sing In<a/>')


    mainDiv.appendChild(conteiner)
} 
